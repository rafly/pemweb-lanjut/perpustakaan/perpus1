<?php

namespace Tests\Feature;

use App\Models\Buku;
use App\Models\Penerbit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BukuTest extends TestCase
{
    use RefreshDatabase;
    public function test_buku_index1()
    {
        $this->withoutExceptionHandling();
        $penerbit = Penerbit::factory()->make();
        $penerbit->save();
        $buku = Buku::factory()->make(['judul' => 'buku 1', 'penerbit_id' => $penerbit->id]);
        $buku->save();
        $response = $this->json('get', '/api/buku/');
        $response->assertStatus(200);
        $response->assertJsonPath('0.judul', $buku->judul);
    }

    public function test_buku_index2()
    {
        $penerbit = Penerbit::factory()->make();
        $penerbit->save();
        $buku = Buku::factory()->make(['judul' => 'buku1', 'penerbit_id' => $penerbit->id]);
        $buku->save();
        $response = $this->get('/api/buku');
        $response->assertStatus(200);
        $response->assertJsonStructure([['judul', 'penerbit_id', 'sampul']]);
    }

    public function test_buku_add()
    {
        $penerbit = Penerbit::factory()->make();
        $penerbit->save();
        $response = $this->postJson('/api/buku', ['judul' => 'buku 1', 'penerbit_id' => $penerbit->id, 'sampul' => '123123']);
        $response->dump()->assertStatus(201);
        $response->assertJsonStructure(['judul', 'penerbit_id', 'sampul']);
    }
}
