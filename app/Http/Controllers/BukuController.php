<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BukuController extends Controller
{
    public function index()
    {
        return Buku::all();
    }

    public function add(Request $request)
    {
        $validatedData = $request->validate([
            'judul' => 'required|string|max:255',
            'sampul' => 'required|string|max:255',
            'penerbit_id' => 'required|exists:penerbit,id',
        ]);

        $buku = new Buku([
            'judul' => $validatedData['judul'],
            'sampul' => $validatedData['sampul'],
            'penerbit_id' => $validatedData['penerbit_id'],
        ]);

        $buku->save();

        return $buku;
    }

    public function detail($id)
    {
        return Buku::find($id);
    }

    public function penerbit($id)
    {
        return Buku::with('penerbit')->find($id);
    }

    public function update(Request $request, $id)
    {

        $buku =  Buku::find($id);
        $buku->update([
            'judul' => $request->judul,
            'sampul' => $request->sampul,
            'penerbit_id' => $request->penerbit_id
        ]);
        return $buku;
    }
}
