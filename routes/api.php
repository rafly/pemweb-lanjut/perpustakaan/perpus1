<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\PenerbitController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/buku','App\Http\Controllers\BukuController@index');
Route::post('/buku','App\Http\Controllers\BukuController@add');
Route::get('/buku/{id}','App\Http\Controllers\BukuController@penerbit');
Route::get('/buku/{id}/penerbit','App\Http\Controllers\BukuController@penerbit');
Route::put('/buku/{id}','App\Http\Controllers\BukuController@update');
Route::get('/penerbit','App\Http\Controllers\PenerbitController@index');
